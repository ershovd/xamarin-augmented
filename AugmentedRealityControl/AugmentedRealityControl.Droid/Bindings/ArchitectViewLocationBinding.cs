using System;
using AugmentedRealityControl.Models;
using MvvmCross.Binding;
using MvvmCross.Binding.Droid.Target;
using Wikitude.Architect;

namespace AugmentedRealityControl.Droid.Bindings
{
    /// <summary>
    /// Binds the Wikitude Droid AR view with the current geo location data.
    /// </summary>
    public class ArchitectViewLocationBinding : MvxAndroidTargetBinding
    {
        protected ArchitectView ArchitectView => (ArchitectView) Target;

        public ArchitectViewLocationBinding(object target) : base(target)
        {
        }

        public override Type TargetType => typeof (Coordinate);

        public override MvxBindingMode DefaultMode => MvxBindingMode.OneWay;

        protected override void SetValueImpl(object target, object value)
        {
            if (ArchitectView == null)
            {
                return;
            }

            var coordinate = value as Coordinate;
            if (coordinate == null)
            {
                return;
            }

            ArchitectView.SetLocation(coordinate.Latitude, coordinate.Longitude,
                coordinate.Altitude, coordinate.Accuracy);
        }
    }
}
