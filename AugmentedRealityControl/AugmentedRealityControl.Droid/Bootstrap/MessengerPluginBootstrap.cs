using MvvmCross.Platform.Plugins;

namespace AugmentedRealityControl.Droid.Bootstrap
{
    public class MessengerPluginBootstrap
        : MvxPluginBootstrapAction<MvvmCross.Plugins.Messenger.PluginLoader>
    {
    }
}