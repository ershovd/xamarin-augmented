﻿if (!this.XR) {
  this.XR = {};
};

(function(exports) {
  "use strict";

  var resources = {};

  resources.indicatorImage = new AR.ImageResource("assets/indi.png");
  resources.indicatorScale = 0.1;
  resources.labelOffsetY = 1;
  resources.labelTextColor = "#FFFFFF";

  exports.resources = resources;
})(this.XR);
