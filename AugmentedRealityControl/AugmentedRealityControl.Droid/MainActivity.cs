﻿using System;
using System.Threading.Tasks;
using Android.App;
using Android.Content.PM;
using Android.OS;
using Android.Util;
using AugmentedRealityControl.Utils;
using AugmentedRealityControl.ViewModels;
using MvvmCross.Droid.Views;
using Wikitude.Architect;

namespace AugmentedRealityControl.Droid
{
    [Activity(Label = "AugmentedRealityControl", Icon = "@drawable/icon", MainLauncher = true,
        ConfigurationChanges = ConfigChanges.ScreenSize | ConfigChanges.Orientation)]
    public class MainActivity : MvxActivity<AugmentedRealityViewModel>
    {
        protected ArchitectView ArchitectView;

        protected override void OnCreate(Bundle bundle)
        {
            base.OnCreate(bundle);

            SetContentView(Resource.Layout.MainViewLayout);
            ArchitectView = FindViewById<ArchitectView>(Resource.Id.ArchitectView);
            var startupConfiguration = new StartupConfiguration(Constants.WikitudeKey,
                Constants.WikitudeRequiredFeatures);
            ArchitectView.OnCreate(startupConfiguration);
        }

        protected override void OnPostCreate(Bundle savedInstanceState)
        {
            base.OnPostCreate(savedInstanceState);

            if (ArchitectView == null) return;

            ArchitectView.OnPostCreate();
            try
            {
                ArchitectView.Load(Constants.WikitudeWorldUrl);
            }
            catch (Exception ex)
            {
                Log.Error("WIKITUDE_SAMPLE", ex.ToString());
            }
        }

        protected override void OnResume()
        {
            base.OnResume();

            ArchitectView?.OnResume();
        }

        protected override void OnPause()
        {
            base.OnPause();

            ArchitectView?.OnPause();
        }

        protected override void OnDestroy()
        {
            base.OnDestroy();

            ArchitectView?.OnDestroy();
        }
    }
}
