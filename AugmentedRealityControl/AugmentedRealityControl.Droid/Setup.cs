using Android.Content;
using AugmentedRealityControl.Droid.Bindings;
using MvvmCross.Binding.Bindings.Target.Construction;
using MvvmCross.Core.ViewModels;
using MvvmCross.Droid.Platform;
using Wikitude.Architect;

namespace AugmentedRealityControl.Droid
{
    public class Setup : MvxAndroidSetup
    {
        public Setup(Context applicationContext) : base(applicationContext)
        {
        }

        protected override IMvxApplication CreateApp()
        {
            return new App();
        }

        protected override void FillTargetFactories(IMvxTargetBindingFactoryRegistry registry)
        {
            base.FillTargetFactories(registry);

            registry.RegisterFactory(
                new MvxCustomBindingFactory<ArchitectView>(
                    "Location",
                    view => new ArchitectViewLocationBinding(view)));

            registry.RegisterFactory(
                new MvxCustomBindingFactory<ArchitectView>(
                    "ItemsSource",
                    view => new ArchitectViewItemsSourceBinding(view)));
        }
    }
}
