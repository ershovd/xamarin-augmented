﻿using System;
using AugmentedRealityControl.Utils;
using Foundation;
using UIKit;
using Wikitude.Architect;
using MvvmCross.Binding.BindingContext;
using MvvmCross.iOS.Views;
using AugmentedRealityControl.ViewModels;

namespace AugmentedRealityControl.iOS
{
	/// <summary>
	/// Main iOS controller for AR viewmodel interaction
	/// </summary>
	public partial class ArchitectViewMain : MvxViewController<AugmentedRealityViewModel>
	{
		private WTArchitectView architectView;
		private WTNavigation navigation;

		static bool UserInterfaceIdiomIsPhone
		{
			get { return UIDevice.CurrentDevice.UserInterfaceIdiom == UIUserInterfaceIdiom.Phone; }
		}

		public ArchitectViewMain() : base ("ArchitectViewMain", null)
		{
		}

		public override void DidReceiveMemoryWarning()
		{
			// Releases the view if it doesn't have a superview.
			base.DidReceiveMemoryWarning();

			// Release any cached data, images, etc that aren't in use.
		}

		public void StartAR()
		{
			if (architectView != null && !architectView.IsRunning)
			{
				architectView.Start(null, null);
				Console.WriteLine("Wikitude SDK version " + Wikitude.Architect.WTArchitectView.SDKVersion + " is running.");
			}
		}

		public void StopAR()
		{
			if (architectView != null && architectView.IsRunning)
			{
				architectView.Stop();
			}
		}

		#region View lifecycle

		public override void ViewDidLoad()
		{
			base.ViewDidLoad();
			// Perform any additional setup after loading the view, typically from a nib.

			this.architectView = new Wikitude.Architect.WTArchitectView();
			this.View.AddSubview(this.architectView);

			this.architectView.TranslatesAutoresizingMaskIntoConstraints = false;

			NSDictionary views = new NSDictionary(new NSString("architectView"), architectView);
			this.View.AddConstraints(NSLayoutConstraint.FromVisualFormat("|[architectView]|", 0, null, views));
			this.View.AddConstraints(NSLayoutConstraint.FromVisualFormat("V:|[architectView]|", 0, null, views));

			architectView.SetLicenseKey(Constants.WikitudeKey);

			// TODO: setup binding here
			var set = this.CreateBindingSet<ArchitectViewMain, AugmentedRealityViewModel>();
			set.Bind(this.architectView).For("Location").To(vm => vm.Coordinate);
			set.Bind(this.architectView).For("ItemsSource").To(vm => vm.ArGeoModels);
			set.Apply();


			NSNotificationCenter.DefaultCenter.AddObserver(UIApplication.DidBecomeActiveNotification, (notification) =>
			{
				if (navigation.Interrupted)
				{
					architectView.reloadArchitectWorld();
				}
				StartAR();
			});

			NSNotificationCenter.DefaultCenter.AddObserver(UIApplication.WillResignActiveNotification, (notification) =>
			{
				StopAR();
			});

			var path = NSBundle.MainBundle.BundleUrl.AbsoluteString + Constants.WikitudeWorldUrl;
			navigation = architectView.LoadArchitectWorldFromURL(NSUrl.FromString(path), (Wikitude.Architect.WTFeatures)Constants.WikitudeRequiredFeatures);
		}

		public override void ViewWillAppear(bool animated)
		{
			base.ViewWillAppear(animated);
			StartAR();
		}

		public override void ViewDidAppear(bool animated)
		{
			base.ViewDidAppear(animated);
		}

		public override void ViewWillDisappear(bool animated)
		{
			base.ViewWillDisappear(animated);
		}

		public override void ViewDidDisappear(bool animated)
		{
			base.ViewDidDisappear(animated);
			StopAR();
		}

		#endregion

		#region Rotation

		public override void WillRotate(UIInterfaceOrientation toInterfaceOrientation, double duration)
		{
			base.WillRotate(toInterfaceOrientation, duration);

			architectView.SetShouldRotateToInterfaceOrientation(true, toInterfaceOrientation);
		}

		public override bool ShouldAutorotate()
		{
			return true;
		}

		public override UIInterfaceOrientationMask GetSupportedInterfaceOrientations()
		{
			return UIInterfaceOrientationMask.All;
		}

		#endregion

	}
}


