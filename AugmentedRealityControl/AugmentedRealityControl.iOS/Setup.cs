using AugmentedRealityControl.iOS.Bindings;
using Foundation;
using MvvmCross.Binding.Bindings.Target.Construction;
using MvvmCross.Core.ViewModels;
using MvvmCross.iOS.Platform;
using MvvmCross.iOS.Views.Presenters;
using MvvmCross.Platform;
using UIKit;
using Wikitude.Architect;

namespace AugmentedRealityControl.iOS
{
    public class Setup : MvxIosSetup
	{
		public Setup(MvxApplicationDelegate appDelegate, IMvxIosViewPresenter presenter) : base(appDelegate, presenter)
		{
		}

		protected override IMvxApplication CreateApp()
		{
			return new App();
		}

		protected override void FillTargetFactories(IMvxTargetBindingFactoryRegistry registry)
		{
			base.FillTargetFactories(registry);

			registry.RegisterFactory(
				new MvxCustomBindingFactory<WTArchitectView>(
					"Location",
					view => new ArchitectViewLocationBinding(view)));

			registry.RegisterFactory(
				new MvxCustomBindingFactory<WTArchitectView>(
					"ItemsSource",
					view => new ArchitectViewItemsSourceBinding(view)));
		}
	}
}
