﻿using AugmentedRealityControl.iOS.Bindings;
using Foundation;
using MvvmCross.Binding.Bindings.Target.Construction;
using MvvmCross.Core.ViewModels;
using MvvmCross.iOS.Platform;
using MvvmCross.iOS.Views.Presenters;
using MvvmCross.Platform;
using UIKit;

namespace AugmentedRealityControl.iOS
{
    // The UIApplicationDelegate for the application. This class is responsible for launching the 
    // User Interface of the application, as well as listening (and optionally responding) to 
    // application events from iOS.
    [Register("AppDelegate")]
    public partial class AppDelegate : MvxApplicationDelegate
    {
		UIWindow window;

		public override bool FinishedLaunching(UIApplication app, NSDictionary options)
		{
			window = new UIWindow(UIScreen.MainScreen.Bounds);

			var presenter = new MvxIosViewPresenter(this, window);

			var setup = new Setup(this, presenter);
			setup.Initialize();

			var startup = Mvx.Resolve<IMvxAppStart>();
			startup.Start();

			window.MakeKeyAndVisible();

			return true;
		}


		// This method is invoked when the application is about to move from active to inactive state.
		// OpenGL applications should use this method to pause.
		public override void OnResignActivation(UIApplication application)
		{
			var vc = (ArchitectViewMain)((UINavigationController)window.RootViewController).VisibleViewController;
			vc.StopAR();
		}

		public override void OnActivated(UIApplication application)
		{
			var vc = (ArchitectViewMain)((UINavigationController)window.RootViewController).VisibleViewController;
			vc.StartAR();
		}
    }

	
}
