using System;
using System.Collections.Generic;
using System.ComponentModel;
using AugmentedRealityControl.Models;
using MvvmCross.Binding;
using MvvmCross.Binding.Bindings.Target;
using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;
using Wikitude.Architect;

namespace AugmentedRealityControl.iOS.Bindings
{
    /// <summary>
    /// Binds the Wikitude iOS AR view with the source collection of models.
    /// </summary>
    public class ArchitectViewItemsSourceBinding : MvxTargetBinding
    {
        private IEnumerable<ArGeoModel> _itemsSource; 
		protected WTArchitectView ArchitectView => (WTArchitectView) Target;

        public ArchitectViewItemsSourceBinding(object target) : base(target)
        {
        }

        public override Type TargetType => typeof (IEnumerable<ArGeoModel>);

        public override MvxBindingMode DefaultMode => MvxBindingMode.OneWay;

		public override void SetValue(object value)
		{
			if (ArchitectView == null)
			{
				return;
			}

			var itemsSource = value as IEnumerable<ArGeoModel>;
			if (itemsSource == null)
			{
				return;
			}

			// TODO: Change subscription implementation.
			if (_itemsSource != null)
			{
				foreach (var item in _itemsSource)
				{
					item.PropertyChanged -= ItemPropertyChanged;
				}
			}

			foreach (var item in itemsSource)
			{
				item.PropertyChanged += ItemPropertyChanged;
			}

			_itemsSource = itemsSource;
			// TODO: Move serialization out of here.
			var itemsSourceJson = JsonConvert.SerializeObject(itemsSource, Formatting.None, new JsonSerializerSettings
			{
				ContractResolver = new CamelCasePropertyNamesContractResolver()
			});

			ArchitectView.CallJavaScript($"World.loadModels('{itemsSourceJson}')");
		}

        private void ItemPropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            var item = sender as ArGeoModel;
            if (item == null)
            {
                return;
            }

            if (e.PropertyName == nameof(ArGeoModel.Enabled))
            {
                // TODO: Move serialization out of here.
                var json = JsonConvert.SerializeObject(new
                {
                    item.Id,
                    item.Enabled
                }, Formatting.None, new JsonSerializerSettings
                {
                    ContractResolver = new CamelCasePropertyNamesContractResolver()
                });

                ArchitectView.CallJavaScript($"World.toggleSmartObjectById('{json}')");
            }
        }
    }
}
