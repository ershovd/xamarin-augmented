using System;
using AugmentedRealityControl.Models;
using MvvmCross.Binding;
using MvvmCross.Binding.Bindings.Target;
using Wikitude.Architect;

namespace AugmentedRealityControl.iOS.Bindings
{
	/// <summary>
	/// Binds the Wikitude iOS AR view with the current geo location data.
	/// </summary>
	public class ArchitectViewLocationBinding : MvxTargetBinding
    {
        protected WTArchitectView ArchitectView => (WTArchitectView) Target;

        public ArchitectViewLocationBinding(object target) : base(target)
        {
        }

        public override Type TargetType => typeof (Coordinate);

        public override MvxBindingMode DefaultMode => MvxBindingMode.OneWay;

		public override void SetValue(object value)
        {
            if (ArchitectView == null)
            {
                return;
            }

            var coordinate = value as Coordinate;
            if (coordinate == null)
            {
                return;
            }

			ArchitectView.InjectLocation(coordinate.Latitude, coordinate.Longitude,
                coordinate.Altitude, coordinate.Accuracy);
        }
    }
}
