﻿if (!this.XR) {
  this.XR = {};
};

(function(exports) {
  "use strict";

  /**
   * Represents a smart object, which wraps multiple AR objects and helps to interact with them as with single unit.
   * @constructor 
   * @param {object} settings - Initial object settings.
   */
  function SmartObject(settings) {
    if (!settings) {
      throw "settings undefined";
    }

    this.id = settings.id;
    this.listeners = settings.listeners;

    if (!settings.location || !(settings.location instanceof AR.GeoLocation)) {
      throw "location undefined";
    }
    this.location = settings.location;

    var cam = [];

    if (!settings.modelPath) {
      throw "modelPath undefined";
    }
    this.model = new AR.Model(settings.modelPath, {
      scale: {
        x: 1,
        y: 1,
        z: 1
      }
    });
    cam.push(this.model);

    if (settings.label) {
      this.label = new AR.Label(settings.label, 1, {
        zOrder: 1,
        offsetY: XR.resources.labelOffsetY,
        style: {
          textColor: XR.resources.labelTextColor,
          fontStyle: AR.CONST.FONT_STYLE.BOLD
        }
      });
      cam.push(this.label);
    };

    this.indicator = new AR.ImageDrawable(XR.resources.indicatorImage, XR.resources.indicatorScale, {
      verticalAnchor: AR.CONST.VERTICAL_ANCHOR.TOP
    });

    this.geoObject = new AR.GeoObject(this.location, {
      drawables: {
        cam: cam,
        indicator: [this.indicator]
      },
      onEnterFieldOfVision: this.onEnterFieldOfVision.bind(this),
      onExitFieldOfVision: this.onExitFieldOfVision.bind(this)
    });

    this.enabled = this.geoObject.enabled = !!settings.enabled;
  };

  /**
   * Destroys underlying AR objects.
   */
  SmartObject.prototype.destroy = function() {
    this.model.destroy();
    this.label.destroy();
    this.indicator.destroy();
    this.geoObject.destroy();
  };

  /**
   * Enables or disables object.
   * @param {bool} disable - Flag value.
   */
  SmartObject.prototype.disable = function(disable) {
    this.enabled = this.visible = this.geoObject.enabled = !disable;
  };

  /** Handles enter in the field of vision. */
  SmartObject.prototype.onEnterFieldOfVision = function() {
    this.visible = true;
    if (this.listeners && this.listeners.visibilityChange) {
      this.listeners.visibilityChange(this.visible);
    }
  };

  /** Handles exit out of the field of vision. */
  SmartObject.prototype.onExitFieldOfVision = function() {
    this.visible = false;
    if (this.listeners && this.listeners.visibilityChange) {
      this.listeners.visibilityChange(this.visible);
    }
  };

  exports.SmartObject = SmartObject;
})(this.XR);
