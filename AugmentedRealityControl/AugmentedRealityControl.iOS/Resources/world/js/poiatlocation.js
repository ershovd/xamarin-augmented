var World = {
  earth: null,

  // called to inject new POI data
  loadPoisFromJsonData: function loadPoisFromJsonDataFn(poiData) {
    /* Earth model */
    var modelEarth = new AR.Model("assets/earth.wt3", {
      scale: {
        x: 100,
        y: 100,
        z: 100
      }
    });

    var location = new AR.GeoLocation(poiData.latitude, poiData.longitude, poiData.altitude);

    World.earth = new AR.GeoObject(location, {
      drawables: {
        cam: [modelEarth]
      }
    });

    World.updateStatusMessage("1 place loaded");
  },

  // updates status message shon in small "i"-button aligned bottom center
  updateStatusMessage: function updateStatusMessageFn(message, isWarning) {

    var themeToUse = isWarning ? "e" : "c",
      iconToUse = isWarning ? "alert" : "info";

    $("#status-message").html(message);
    $("#popupInfoButton").buttonMarkup({
      theme: themeToUse
    });
    $("#popupInfoButton").buttonMarkup({
      icon: iconToUse
    });
  },

  // location updates, fired every time you call architectView.setLocation() in native environment
  locationChanged: function locationChangedFn(lat, lon, alt, acc) {

    $("#messageTitle").html("lon: " + lon + ", lat: " + lat);

    if (!World.earth) {
      // creates a poi object with a random location near the user's location
      var poiData = {
        "id": 1,
        "longitude": (lon + (Math.random() / 30)),
        "latitude": (lat + (Math.random() / 30)),
        "altitude": 100.0
      };

      World.loadPoisFromJsonData(poiData);
    }
  }
};

AR.context.onLocationChanged = World.locationChanged;