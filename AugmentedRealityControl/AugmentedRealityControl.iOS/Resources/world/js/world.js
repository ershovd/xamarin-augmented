if (!this.XR) {
  this.XR = {};
};

(function(exports) {
  "use strict";

  /**
  * Represents world object, which provides a single access point for control over AR models.
  * @constructor
  * @param {string} title - The title of the book.
  * @param {string} author - The author of the book.
  */
  function World() {
    this.display = new Display();

    this.startLocation = void 0;
    this.currentLocation = void 0;

    this.smartObjects = [];

    AR.context.onLocationChanged = this.locationChanged.bind(this);
  };

  /** Popuplates world with objects.
  * @param {string} modelsJson - Array with collection of geo models encoded in the JSON format.
  */
  World.prototype.loadModels = function(modelsJson) {
    var me = this,
      errors = [];

    if (me.smartObjects.length) {
      $.each(me.smartObjects, function(index, smartObject) {
        smartObject.destroy();
      });
      me.smartObjects = [];
      me.display.toggleViewportBorder(false);
    }

    if (modelsJson) {
      var models = $.parseJSON(modelsJson);

      $.each(models, function(index, model) {
        try {
          var smartObject = new XR.SmartObject({
            id: model.id,
            label: model.label,
            location: new AR.GeoLocation(model.location.latitude, model.location.longitude, model.location.altitude),
            modelPath: model.modelPath,
            listeners: {
              visibilityChange: me.visibilityChange.bind(me)
            },
            enabled: model.enabled
          });

          me.smartObjects.push(smartObject);
        } catch (error) {
          // TODO: Log this or smth.
          errors.push(error);
        }
      });
    }

    if (errors.length) {
      me.display.showMessage(errors);
    }
  };

  /**
   * Toggles enable/disable flag of the world smart object by it's id.
   * @param {string} toggleOptionsJson - Object with toggle options encoded in the JSON format.
   */
  World.prototype.toggleSmartObjectById = function(toggleOptionsJson) {
    var me = this;

    if (toggleOptionsJson) {
      var toggleOptions = $.parseJSON(toggleOptionsJson);

      $.each(me.smartObjects, function(index, smartObject) {
        if (smartObject.id === toggleOptions.id) {
          smartObject.disable(!toggleOptions.enabled);
        }
      });
      this.visibilityChange();
    }
  };

  /** Handles change of the geo location.
  * @param {float} lat - Latitude.
  * @param {float} lon - Longitude.
  * @param {float} alt - Altitude.
  * @param {float} alt - Accuracy.
  */
  World.prototype.locationChanged = function(lat, lon, alt, acc) {
    var location = new AR.GeoLocation(lat, lon, alt);

    if (!this.startLocation) {
      this.startLocation = location;
      this.display.showPopupMessage("World is ready!", false);
    }
    if (this.currentLocation) {
      this.currentLocation.destroy();
    }
    this.currentLocation = location;
  };

  /** Handles enter in the field of vision. */
  World.prototype.visibilityChange = function () {
    var toggle = false;

    $.each(this.smartObjects, function(index, smartObject) {
      if (smartObject.visible) {
        toggle = true;
        return false;
      }
      return true;
    });

    this.display.toggleViewportBorder(toggle);
  };

  /**
  * Provides a set of methods to interact with UI using jQuery.
  * @constructor
  */
  function Display() {
    this.$viewport = $("#viewport");
    this.$message = $("#message");
    this.$popupButton = $("#popupInfoButton");
    this.$popupMessage = $("#popup-message");
  };

  /**
   * Shows text in the message div.
   * @param {string} message - Message text.
   */
  Display.prototype.showMessage = function(message) {
    if (Array.isArray(message)) {
      message = message.join("<br />");
    }

    this.$message.html(message);
  };

  /**
   * Shows text and alert/info icon in the popup info div.
   * @param {string} message - Message text.
   * @param {bool} isWarning - Boolean flag to switch alert/info icon.
   */
  Display.prototype.showPopupMessage = function(message, isWarning) {
    var themeToUse = isWarning ? "e" : "c",
      iconToUse = isWarning ? "alert" : "info";

    this.$popupMessage.html(message);
    this.$popupButton.buttonMarkup({
      theme: themeToUse
    });
    this.$popupButton.buttonMarkup({
      icon: iconToUse
    });
  };

  /**
   * Highlights board around viewport div.
   * @param {bool} toggle - Boolean flag to toggle border highlight color.
   */
  Display.prototype.toggleViewportBorder = function(toggle) {
    this.$viewport.toggleClass("viewport-border-active", toggle);
  };

  exports.World = World;
})(this.XR);
