﻿using System;

namespace AugmentedRealityControl.Models
{
    /// <summary>
    /// Represents geo location data.
    /// </summary>
    public class Coordinate
    {
        private static readonly Random Random = new Random(DateTime.Now.Millisecond);
        private const double RandomPower = 0.001d;

        /// <summary>
        /// Gets or sets the latitude.
        /// </summary>
        public double Latitude { get; set; }

        /// <summary>
        /// Gets or sets the longitude.
        /// </summary>
        public double Longitude { get; set; }

        /// <summary>
        /// Gets or sets the altitude in meters relative to sea level.
        /// </summary>
        public double Altitude { get; set; }

        /// <summary>
        /// Gets or sets the potential position error radius in meters.
        /// </summary>
        public float Accuracy { get; set; }

        /// <summary>
        /// Creates random coordinate nearby.
        /// </summary>
        public Coordinate NewNearby()
        {
            return new Coordinate
            {
                Latitude = Latitude + Random.NextDouble()*RandomPower,
                Longitude = Longitude + Random.NextDouble()*RandomPower,
                Altitude = Altitude + Random.NextDouble()*RandomPower
            };
        }
    }
}
