﻿using System;
using MvvmCross.Core.ViewModels;

namespace AugmentedRealityControl.Models
{
    /// <summary>
    /// Represents source model for augmented reality control.
    /// </summary>
    public class ArGeoModel : MvxNotifyPropertyChanged
    {
        private string _label;
        private bool _enabled;

        public ArGeoModel(Coordinate location, string modelPath)
        {
            if (location == null)
                throw new ArgumentNullException(nameof(location));

            if (modelPath == null)
                throw new ArgumentNullException(nameof(modelPath));

            Id = Guid.NewGuid();
            Location = location;
            ModelPath = modelPath;
            Enabled = true;
        }

        /// <summary>
        /// Model's unique identifier.
        /// </summary>
        public Guid Id { get; private set; }

        /// <summary>
        /// Geo coordinate of the model.
        /// </summary>
        public Coordinate Location { get; private set; }

        /// <summary>
        /// Path to Wikitude 3D Format file (.wt3), which will be used to render model.
        /// </summary>
        public string ModelPath { get; private set; }

        /// <summary>
        /// Gets or sets label text. If set, label text will be rendered upon model.
        /// </summary>
        public string Label
        {
            get { return _label; }
            set
            {
                if (_label == value) return;

                _label = value;
                RaisePropertyChanged(() => Label);
            }
        }

        /// <summary>
        /// Boolean flag to enable or disable model. If set to false, model won't be considered in the calculations to project its drawables onto the camera screen.
        /// </summary>
        public bool Enabled
        {
            get { return _enabled; }
            set
            {
                if (_enabled == value) return;

                _enabled = value;
                RaisePropertyChanged(() => Enabled);
            }
        }
    }
}
