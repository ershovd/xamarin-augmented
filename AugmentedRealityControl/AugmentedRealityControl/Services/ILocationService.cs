﻿namespace AugmentedRealityControl.Services
{
    /// <summary>
    /// Simple interface for location service.
    /// </summary>
    public interface ILocationService
    {
        /// <summary>
        /// Starts listening for geo location changes.
        /// </summary>
        void Start();

        /// <summary>
        /// Stops listening for geo location changes.
        /// </summary>
        void Stop();
    }
}
