﻿using AugmentedRealityControl.Models;
using MvvmCross.Plugins.Messenger;

namespace AugmentedRealityControl.Services
{
    /// <summary>
    /// Represents service message to pass geo data from location service to consumers.
    /// </summary>
    public class LocationMessage : MvxMessage
    {
        public LocationMessage(object sender, Coordinate coordinate)
            : base(sender)
        {
            Coordinate = coordinate;
        }

        /// <summary>
        /// Geo location data.
        /// </summary>
        public Coordinate Coordinate { get; private set; }
    }
}
