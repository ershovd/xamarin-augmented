﻿using AugmentedRealityControl.Models;
using MvvmCross.Plugins.Messenger;
using Plugin.Geolocator;
using Plugin.Geolocator.Abstractions;

namespace AugmentedRealityControl.Services
{
    /// <summary>
    /// Location service implementation.
    /// </summary>
    public class LocationService : ILocationService
    {
        private readonly IMvxMessenger _messenger;

        public LocationService(IMvxMessenger messenger)
        {
            _messenger = messenger;
            Start();
            CrossGeolocator.Current.PositionChanged += PositionChanged;
        }

        public void Start()
        {
            if (!CrossGeolocator.Current.IsListening)
            {
                CrossGeolocator.Current.StartListeningAsync(1, 1d, true);
            }
        }

        public void Stop()
        {
            CrossGeolocator.Current.StopListeningAsync();
        }

        private void PositionChanged(object sender, PositionEventArgs e)
        {
            var message = new LocationMessage(this, new Coordinate
            {
                Longitude = e.Position.Longitude,
                Latitude = e.Position.Latitude,
                Altitude = e.Position.Altitude,
                Accuracy = (float) e.Position.Accuracy
            });

            _messenger.Publish(message);
        }
    }
}
