﻿using System.Collections.Generic;
using AugmentedRealityControl.Models;
using AugmentedRealityControl.Services;
using AugmentedRealityControl.Utils;
using MvvmCross.Core.ViewModels;
using MvvmCross.Plugins.Messenger;

namespace AugmentedRealityControl.ViewModels
{
    /// <summary>
    /// Provides bindable properties and methods for the Wikitude Xamarin views.
    /// </summary>
    public class AugmentedRealityViewModel : MvxViewModel
    {
        private readonly MvxSubscriptionToken _token;

        private Coordinate _coordinate;
        private IEnumerable<ArGeoModel> _arGeoModels;

        public AugmentedRealityViewModel(IMvxMessenger messenger)
        {
            _token = messenger.Subscribe<LocationMessage>(OnLocationMessage);
        }

        /// <summary>
        /// Gets or sets current location data.
        /// </summary>
        public Coordinate Coordinate
        {
            get { return _coordinate; }
            set
            {
                if (_coordinate == value) return;

                _coordinate = value;
                RaisePropertyChanged(() => Coordinate);
            }
        }

        /// <summary>
        /// Gets or sets source collection of models for the Wikitude Xamarin view.
        /// </summary>
        public IEnumerable<ArGeoModel> ArGeoModels
        {
            get { return _arGeoModels; }
            set
            {
                if (_arGeoModels == value) return;

                _arGeoModels = value;
                RaisePropertyChanged(() => ArGeoModels);
            }
        }

        /// <summary>
        /// Processes location messages from location service.
        /// </summary>
        /// <param name="locationMessage">Location message.</param>
        private void OnLocationMessage(LocationMessage locationMessage)
        {
            Coordinate = locationMessage.Coordinate;

            if (ArGeoModels != null) return;

            ArGeoModels = new List<ArGeoModel>
            {
                new ArGeoModel(Coordinate.NewNearby(), Constants.EarthModelPath)
                {
                    Label = "First Model",
                    Enabled = false
                },
                new ArGeoModel(Coordinate.NewNearby(), Constants.EarthModelPath)
                {
                    Label = "Second Model"
                },
                new ArGeoModel(Coordinate.NewNearby(), Constants.EarthModelPath)
                {
                    Label = "Third Model"
                }
            };
        }
    }
}
