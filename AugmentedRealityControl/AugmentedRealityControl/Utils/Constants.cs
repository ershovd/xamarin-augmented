namespace AugmentedRealityControl.Utils
{
    /// <summary>
    /// Represents constants used to configure the Wikitude Xamarin views.
    /// </summary>
    public static class Constants
    {
        public const string WikitudeKey =
            "qqdVvoPsVBGbfr0t4wFa0tXBrK3NiVklngPfKy6/sMabY/aKsDZ/VyCtok6DKYfMEjvlthlnPrBBswlU2Svp03vSdGH+6L1XnQf/4T7/5wonrljAojtUmlKWoUX6SRylpWXzFZpMcHWryNiH6Va+huxlFNCSIfzJ3vkZd0q+DGdTYWx0ZWRfX+oyAqXMRkTG1a6ePyvBfl5wqY/PE2W4MBwISaRCnHTnhrUpa0df4vO4bJH6/u/SfcpEKZiLW/kIq6O8AA2itcNe0JWoovZw4NUXNjR65P8JCiiS9N4JrknQjuAEBfKci4oOVqeHaMRGaEAM4IRky0ZCValwISyf6OPlhFIQd+5T+8xYgxFboaPNK55ydL3cK7BWdEMiXewQw1smjfH0DRshMq00nGOZqe/GSaOjlhgwQY6eL5iPKz6ayneoMJWNBqXzcUlhaG8KhlaG6WNUSR3JnzrtrSl1V42n7V/kujIw6u8SLcYDaGHsH/5y2nLXsXybxcj7SrcbcHVUhC7nMyskjjfBdVKeHlP/umIY2AO9xr0DC3e/jjhULusoDfwGg/Hg6ahiZ+N2E39hMgLIVGJBPaDWXx1PYgD20vFpypqnGfUPKaHVqtlt41eBsEQGrfzV2B1LGCGRz/IIwFAQTTeYUkzxVrGHWLLlSHjKsBXyveYoHfq5Lv8=";

        public const int WikitudeRequiredFeatures = 3;

        public const string WikitudeWorldUrl = "world/index.html";

        public const string EarthModelPath = "assets/earth.wt3";
    }
}
